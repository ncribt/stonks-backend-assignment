use serde::Deserialize;

fn default_host() -> String {
    "0.0.0.0".to_string()
}

fn default_port() -> String {
    "3000".to_string()
}

#[derive(Debug, Deserialize)]
pub struct Env {
    #[serde(default = "default_host")]
    pub host: String,
    #[serde(default = "default_port")]
    pub port: String,
    pub database_url: String,
    pub session_secret: String,
}

impl Env {
    pub fn from_env() -> Self {
        dotenvy::dotenv().ok();

        envy::from_env::<Env>()
            .map_err(|err| match err {
                envy::Error::MissingValue(v) => {
                    format!(r#"Missing env var: "{}""#, v.to_uppercase())
                }
                envy::Error::Custom(err) => format!(
                    "An unexpected error occurred while parsing the environment variables: {err:?}"
                ),
            })
            .unwrap()
    }
}
