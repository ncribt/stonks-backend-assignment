use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use utoipa::ToSchema;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, FromRow, ToSchema)]
#[serde(rename_all = "camelCase")]
pub struct Profile {
    pub id: Uuid,
    pub full_name: String,
    pub username: String,
    pub email: String,
    pub password: String,
    pub avatar: String,
    pub active: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Deserialize, ToSchema)]
#[serde(rename_all = "camelCase")]
pub struct ProfileInsert {
    pub full_name: String,
    pub username: String,
    pub email: String,
    pub password: String,
    pub avatar: String,
    pub active: bool,
}

#[derive(Debug, Clone, Deserialize, ToSchema)]
#[serde(rename_all = "camelCase")]
pub struct ProfileUpdate {
    pub full_name: Option<String>,
    pub username: Option<String>,
    pub email: Option<String>,
    pub password: Option<String>,
    pub avatar: Option<String>,
    pub active: Option<bool>,
}

#[derive(Debug, Clone, FromRow)]
pub struct SuperAdmin {
    pub id: i32,
    pub profile_id: Uuid,
}

#[derive(Debug, Clone, FromRow)]
pub struct Mfa {
    pub id: i32,
    pub profile_id: Uuid,
    pub code: Option<String>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, FromRow)]
pub struct Follow {
    pub id: i32,
    pub follower_id: Uuid,
    pub following_id: Uuid,
}
