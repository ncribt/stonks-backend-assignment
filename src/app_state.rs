use std::{collections::HashMap, sync::Arc};

use sqlx::{Pool, Postgres, postgres::PgPoolOptions};
use tokio::sync::Mutex;

use crate::controller::ws::ChannelMap;

pub struct AppState {
    pub pool: Pool<Postgres>,
    pub channels: ChannelMap,
}

impl AppState {
    pub async fn new(database_url: &str) -> Self {
        let pool = PgPoolOptions::new()
            .max_connections(5)
            .connect(database_url)
            .await
            .unwrap();

        AppState {
            pool,
            channels: Arc::new(Mutex::new(HashMap::new())),
        }
    }
}
