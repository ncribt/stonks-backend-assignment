use sqlx::{Pool, Postgres, query, query_as, query_scalar};
use uuid::Uuid;

use crate::model::channel::{Channel, ChannelInsert, ChannelUpdate};

pub async fn select_channel(
    id: &Uuid,
    pool: &Pool<Postgres>,
) -> Result<Option<Channel>, sqlx::Error> {
    query_as!(Channel, r#"SELECT * FROM channel WHERE id = $1"#, id)
        .fetch_optional(pool)
        .await
}

pub async fn insert_channel(
    payload: &ChannelInsert,
    pool: &Pool<Postgres>,
) -> Result<Uuid, sqlx::Error> {
    query_scalar!(
        r#"
            INSERT INTO channel(title, description, host, suspended)
            VALUES ($1, $2, $3, $4)
            RETURNING id
        "#,
        payload.title,
        payload.description,
        payload.host,
        payload.suspended
    )
    .fetch_one(pool)
    .await
}

pub async fn update_channel(
    id: &Uuid,
    payload: &ChannelUpdate,
    pool: &Pool<Postgres>,
) -> Result<(), sqlx::Error> {
    query!(
        r#"
            UPDATE channel c
            SET
                updated_at = NOW(),
                title = COALESCE($2, c.title),
                description = COALESCE($3, c.description),
                host = COALESCE($4, c.host),
                suspended = COALESCE($5, c.suspended)
            WHERE id = $1
        "#,
        id,
        payload.title,
        payload.description,
        payload.host,
        payload.suspended
    )
    .execute(pool)
    .await?;

    Ok(())
}
