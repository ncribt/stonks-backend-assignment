use std::sync::Arc;

use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::Json;
use serde::Serialize;
use utoipa::ToSchema;
use uuid::Uuid;

use crate::app_state::AppState;
use crate::auth::AuthSession;
use crate::db;
use crate::model::channel::{Channel, ChannelInsert, ChannelUpdate};

#[utoipa::path(
    get,
    path = "/channel/{id}",
    params(("id", description = "Channel ID")),
    responses((status = 200, body = Channel))
)]
pub async fn get_channel(
    State(state): State<Arc<AppState>>,
    Path(id): Path<Uuid>,
) -> Result<Json<Option<Channel>>, (StatusCode, String)> {
    // TODO: Check if user is banned
    match db::channel::select_channel(&id, &state.pool).await {
        Ok(res) => Ok(Json(res)),
        Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
    }
}

#[derive(Serialize, ToSchema)]
pub struct PostChannelResponse {
    pub id: Uuid,
}

#[utoipa::path(
    post,
    path = "/channel",
    request_body = ChannelInsert,
    responses((status = 200, body = PostChannelResponse))
)]
pub async fn post_channel(
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
    Json(body): Json<ChannelInsert>,
) -> Result<Json<PostChannelResponse>, (StatusCode, String)> {
    match auth_session.user {
        Some(user) => {
            let updated_body = ChannelInsert {
                host: user.id,
                ..body
            };

            match db::channel::insert_channel(&updated_body, &state.pool).await {
                Ok(res) => Ok(Json(PostChannelResponse { id: res })),
                Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
            }
        }
        None => Err((StatusCode::UNAUTHORIZED, "Not logged in".to_string())),
    }
}

#[utoipa::path(
    patch,
    path = "/channel/{id}",
    params(("id", description = "Channel ID")),
    request_body = ChannelUpdate,
    responses((status = 200))
)]
pub async fn patch_channel(
    State(state): State<Arc<AppState>>,
    Path(id): Path<Uuid>,
    Json(body): Json<ChannelUpdate>,
) -> Result<(), (StatusCode, String)> {
    // TODO: Ensure user is either HOST, ADMIN or SUPERADMIN
    // TODO: Ensure only SUPERADMIN can suspend
    // TODO: Ensure only HOST or SUPERADMIN can change the host
    match db::channel::update_channel(&id, &body, &state.pool).await {
        Ok(_) => Ok(()),
        Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
    }
}