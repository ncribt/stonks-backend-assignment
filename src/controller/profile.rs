use std::sync::Arc;

use axum::{
    extract::{Path, State},
    http::StatusCode,
    Json,
};
use serde::Serialize;
use utoipa::ToSchema;
use uuid::Uuid;

use crate::app_state::AppState;
use crate::auth::AuthSession;
use crate::db;
use crate::model::profile::{Profile, ProfileInsert, ProfileUpdate};
use crate::util::hash_password;

#[utoipa::path(
    get,
    path = "/profile/{id}",
    params(("id", description = "Profile ID")),
    responses((status = 200, body = Profile))
)]
pub async fn get_profile(
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
    Path(id): Path<Uuid>,
) -> Result<Json<Option<Profile>>, (StatusCode, String)> {
    if let Some(user) = auth_session.user {
        if user.id != id {
            return Err((StatusCode::FORBIDDEN, "IDs differ".to_string()));
        }
    } else {
        return Err((StatusCode::UNAUTHORIZED, "Not logged in".to_string()));
    }

    match db::profile::select_profile(&id, &state.pool).await {
        Ok(res) => Ok(Json(res)),
        Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
    }
}

#[derive(Serialize, ToSchema)]
pub struct PostProfileResponse {
    pub id: Uuid,
}

#[utoipa::path(
    post,
    path = "/profile",
    request_body = ProfileInsert,
    responses((status = 200, body = PostProfileResponse))
)]
pub async fn post_profile(
    State(state): State<Arc<AppState>>,
    Json(body): Json<ProfileInsert>,
) -> Result<Json<PostProfileResponse>, (StatusCode, String)> {
    let updated_body = ProfileInsert {
        password: match hash_password(&body.password) {
            Ok(res) => res,
            Err(err) => return Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
        },
        ..body
    };

    match db::profile::insert_profile(&updated_body, &state.pool).await {
        Ok(res) => Ok(Json(PostProfileResponse { id: res })),
        Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
    }
}

#[utoipa::path(
    patch,
    path = "/profile/{id}",
    params(("id", description = "Profile ID")),
    request_body = ProfileUpdate,
    responses((status = 200))
)]
pub async fn patch_profile(
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
    Path(id): Path<Uuid>,
    Json(body): Json<ProfileUpdate>,
) -> Result<(), (StatusCode, String)> {
    if let Some(user) = auth_session.user {
        if user.id != id {
            return Err((StatusCode::FORBIDDEN, "IDs differ".to_string()));
        }
    } else {
        return Err((StatusCode::UNAUTHORIZED, "Not logged in".to_string()));
    }

    let updated_body = ProfileUpdate {
        password: if let Some(password) = body.password {
            match hash_password(&password) {
                Ok(res) => Some(res),
                Err(err) => return Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
            }
        } else {
            None
        },
        ..body
    };

    match db::profile::update_profile(&id, &updated_body, &state.pool).await {
        Ok(_) => Ok(()),
        Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
    }
}

#[utoipa::path(
    post,
    path = "/profile/{id}/follow",
    params(("id", description = "Profile ID")),
    responses((status = 200))
)]
pub async fn post_follow(
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
    Path(id): Path<Uuid>,
) -> Result<(), (StatusCode, String)> {
    match auth_session.user {
        Some(user) => match db::profile::insert_follow(&user.id, &id, &state.pool).await {
            Ok(_) => Ok(()),
            Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
        },
        None => Err((StatusCode::UNAUTHORIZED, "Not logged in".to_string())),
    }
}

#[utoipa::path(
    delete,
    path = "/profile/{id}/follow",
    params(("id", description = "Profile ID")),
    responses((status = 200))
)]
pub async fn delete_follow(
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
    Path(id): Path<Uuid>,
) -> Result<(), (StatusCode, String)> {
    match auth_session.user {
        Some(user) => match db::profile::delete_follow(&user.id, &id, &state.pool).await {
            Ok(_) => Ok(()),
            Err(err) => Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
        },
        None => Err((StatusCode::UNAUTHORIZED, "Not logged in".to_string())),
    }
}
