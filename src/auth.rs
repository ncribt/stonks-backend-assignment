use async_trait::async_trait;
use axum_login::{AuthnBackend, AuthUser, UserId};
use serde::{Deserialize, Serialize};
use sqlx::{Pool, Postgres};
use utoipa::ToSchema;
use uuid::Uuid;

use crate::db::profile::{select_profile, select_profile_by_email};
use crate::model::profile::Profile;
use crate::util::{Argon2Error, verify_password};

#[derive(Debug, Clone, Serialize, ToSchema)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    pub password: String,
}

impl From<Profile> for User {
    fn from(value: Profile) -> Self {
        User {
            id: value.id,
            username: value.username,
            password: value.password,
        }
    }
}

impl AuthUser for User {
    type Id = Uuid;

    fn id(&self) -> Self::Id {
        self.id
    }

    fn session_auth_hash(&self) -> &[u8] {
        self.password.as_bytes()
    }
}

#[derive(Clone)]
pub struct Backend {
    pub pool: Pool<Postgres>,
}

impl Backend {
    pub fn new(pool: Pool<Postgres>) -> Self {
        Backend { pool }
    }
}

#[derive(Clone, Debug, Deserialize, ToSchema)]
pub struct Credentials {
    pub email: String,
    pub password: String,
}

#[derive(Debug, thiserror::Error)]
pub enum BackendError {
    #[error(transparent)]
    Database(sqlx::Error),
    #[error(transparent)]
    PasswordVerification(Argon2Error),
}

#[async_trait]
impl AuthnBackend for Backend {
    type User = User;
    type Credentials = Credentials;
    type Error = BackendError;

    async fn authenticate(
        &self,
        Credentials { email, password }: Self::Credentials,
    ) -> Result<Option<Self::User>, Self::Error> {
        let user = select_profile_by_email(&email, &self.pool)
            .await
            .map_err(Self::Error::Database)?
            .map(User::from);

        if let Some(user) = user {
            if verify_password(&password, &user.password)
                .map_err(Self::Error::PasswordVerification)?
            {
                return Ok(Some(user));
            } else {
                return Ok(None);
            }
        } else {
            return Ok(None);
        }
    }

    async fn get_user(&self, user_id: &UserId<Self>) -> Result<Option<Self::User>, Self::Error> {
        Ok(select_profile(user_id, &self.pool)
            .await
            .map_err(Self::Error::Database)?
            .map(User::from))
    }
}

pub type AuthSession = axum_login::AuthSession<Backend>;
