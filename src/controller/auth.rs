use axum::{Form, Json};
use axum::http::StatusCode;

use crate::auth::{AuthSession, Credentials, User};

#[utoipa::path(
    post,
    path = "/login",
    request_body(content = Credentials, content_type = "application/x-www-form-urlencoded"),
    responses((status = 200, body = User))
)]
pub async fn login(
    mut auth_session: AuthSession,
    Form(creds): Form<Credentials>,
) -> Result<Json<User>, (StatusCode, String)> {
    let user = match auth_session.authenticate(creds.clone()).await {
        Ok(Some(user)) => user,
        Ok(None) => return Err((StatusCode::UNAUTHORIZED, "Invalid credentials".to_string())),
        Err(err) => return Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string())),
    };

    if let Err(err) = auth_session.login(&user).await {
        return Err((StatusCode::INTERNAL_SERVER_ERROR, err.to_string()));
    }

    Ok(Json(user))
}
