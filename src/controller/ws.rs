use std::collections::HashMap;
use std::sync::Arc;

use axum::{
    extract::{
        Path,
        State, ws::{Message, WebSocket, WebSocketUpgrade},
    },
    response::IntoResponse,
};
use futures::{
    SinkExt,
    stream::{SplitSink, SplitStream}, StreamExt,
};
use log::debug;
use tokio::sync::{mpsc, Mutex};
use uuid::Uuid;

use crate::app_state::AppState;
use crate::auth::AuthSession;

pub type ChannelId = Uuid;
pub type ChannelUserId = Uuid;

pub struct ChannelUser {
    pub username: String,
    pub tx: mpsc::Sender<String>,
}

pub type ChannelMap = Arc<Mutex<HashMap<ChannelId, HashMap<ChannelUserId, ChannelUser>>>>;

pub async fn ws_handler(
    ws: WebSocketUpgrade,
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
) -> impl IntoResponse {
    println!("New connection");
    ws.on_upgrade(move |socket| handle_socket(socket, auth_session, state.channels.clone()))
}

async fn handle_socket(mut socket: WebSocket, auth_session: AuthSession, channels: ChannelMap) {
    let channel_user_id = Uuid::new_v4();
    let username = auth_session
        .user
        .map(|u| u.username)
        .unwrap_or("Anonymous".to_string());

    println!("Connection established");

    let (mut sender, mut receiver) = socket.split();
    let (tx, rx) = mpsc::channel::<String>(100);

    let mut sending = tokio::spawn(write(sender, rx));
    let mut receiving = tokio::spawn(read(
        receiver,
        channel_user_id,
        username,
        tx.clone(),
        channels,
    ));

    tokio::select! {
        _ = (sending) => (),
        _ = (receiving) => ()
    }

    // TODO: Handle connection close
}

async fn read(
    mut receiver: SplitStream<WebSocket>,
    channel_user_id: Uuid,
    username: String,
    tx: mpsc::Sender<String>,
    channels: ChannelMap,
) {
    let mut current_channel: Option<ChannelId> = None;

    while let Some(Ok(msg)) = receiver.next().await {
        match msg {
            Message::Text(text) => {
                // TODO: Handle other commands
                if let Some(channel_id) = text.strip_prefix("/join ") {
                    if let Ok(channel_id) = Uuid::parse_str(channel_id) {
                        // TODO: Verify that channel exists and user is not banned
                        
                        join_channel(
                            channel_id,
                            channel_user_id,
                            username.clone(),
                            tx.clone(),
                            channels.clone(),
                        )
                        .await;

                        current_channel = Some(channel_id);

                        let _ = tx.send(format!("Joined channel: {}", channel_id)).await;
                    } else {
                        let _ = tx.send("Incorrect channel_id".to_string()).await;
                    }
                } else if let Some(channel_id) = &current_channel {
                    debug!(
                        "Received from {} in channel {}: {}",
                        channel_user_id, channel_id, text
                    );

                    let message = format!("{}: {}", username, text);

                    broadcast(*channel_id, channel_user_id, &message, channels.clone()).await;
                } else {
                    let _ = tx
                        .send(
                            "You need to join a channel first. Use /join <channel_id>".to_string(),
                        )
                        .await;
                }
            }
            Message::Close(_) => break,
            _ => {}
        }
    }
}

async fn write(mut sender: SplitSink<WebSocket, Message>, mut rx: mpsc::Receiver<String>) {
    while let Some(message) = rx.recv().await {
        if let Err(e) = sender.send(Message::Text(message)).await {
            eprintln!("Error sending message: {}", e);
            break;
        }
    }
}

async fn join_channel(
    channel_id: ChannelId,
    channel_user_id: ChannelUserId,
    username: String,
    tx: mpsc::Sender<String>,
    channels: ChannelMap,
) {
    let mut channels = channels.lock().await;
    let channel = channels.entry(channel_id).or_insert_with(HashMap::new);
    channel.insert(channel_user_id, ChannelUser { username, tx });
}

async fn broadcast(
    channel_id: ChannelId,
    sender_id: ChannelUserId,
    message: &str,
    channels: ChannelMap,
) {
    let channels = channels.lock().await;
    if let Some(channel) = channels.get(&channel_id) {
        for (&channel_user_id, user) in channel.iter() {
            if channel_user_id != sender_id {
                if let Err(_) = user.tx.send(message.to_owned()).await {
                    println!(
                        "Failed to send message to {} in channel {}",
                        channel_user_id, channel_id
                    );
                }
            }
        }
    }
}
