use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use utoipa::ToSchema;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, FromRow, ToSchema)]
#[serde(rename_all = "camelCase")]
pub struct Channel {
    pub id: Uuid,
    pub title: String,
    pub description: String,
    pub host: Uuid,
    pub suspended: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Deserialize, FromRow, ToSchema)]
#[serde(rename_all = "camelCase")]
pub struct ChannelInsert {
    pub title: String,
    pub description: String,
    pub host: Uuid,
    pub suspended: bool,
}

#[derive(Debug, Clone, Deserialize, FromRow, ToSchema)]
#[serde(rename_all = "camelCase")]
pub struct ChannelUpdate {
    pub title: Option<String>,
    pub description: Option<String>,
    pub host: Option<Uuid>,
    pub suspended: Option<bool>,
}

#[derive(Debug, Clone, FromRow)]
pub struct ChannelBan {
    pub id: i32,
    pub channel_id: Uuid,
    pub profile_id: Uuid,
}

#[derive(Debug, Clone, FromRow)]
pub struct ChannelMute {
    pub id: i32,
    pub channel_id: Uuid,
    pub profile_id: Uuid,
}

#[derive(Debug, Clone, FromRow)]
pub struct ChannelAdmin {
    pub id: i32,
    pub channel_id: Uuid,
    pub profile_id: Uuid,
}
