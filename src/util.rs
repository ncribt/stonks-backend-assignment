use std::fmt::{Display, Formatter};

use argon2::{
    Argon2,
    password_hash::{PasswordHash, rand_core::OsRng, SaltString}, PasswordHasher, PasswordVerifier,
};

#[derive(Debug)]
pub struct Argon2Error {
    pub msg: String,
}

impl Display for Argon2Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl std::error::Error for Argon2Error {}

impl From<argon2::password_hash::Error> for Argon2Error {
    fn from(value: argon2::password_hash::Error) -> Self {
        Argon2Error {
            msg: value.to_string(),
        }
    }
}

pub fn hash_password(password: &str) -> Result<String, Argon2Error> {
    let salt = SaltString::generate(&mut OsRng);
    let argon2 = Argon2::default();

    Ok(argon2
        .hash_password(password.as_bytes(), &salt)?
        .to_string())
}

pub fn verify_password(password: &str, password_hash: &str) -> Result<bool, Argon2Error> {
    let parsed_hash = PasswordHash::new(password_hash)?;

    Ok(Argon2::default()
        .verify_password(password.as_bytes(), &parsed_hash)
        .is_ok())
}
