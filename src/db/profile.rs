use sqlx::{Pool, Postgres, query, query_as, query_scalar};
use uuid::Uuid;

use crate::model::profile::{Profile, ProfileInsert, ProfileUpdate};

pub async fn select_profile(
    id: &Uuid,
    pool: &Pool<Postgres>,
) -> Result<Option<Profile>, sqlx::Error> {
    query_as!(Profile, r#"SELECT * FROM profile WHERE id = $1"#, id)
        .fetch_optional(pool)
        .await
}

pub async fn select_profile_by_email(
    email: &str,
    pool: &Pool<Postgres>,
) -> Result<Option<Profile>, sqlx::Error> {
    query_as!(
        Profile,
        r#"SELECT * FROM profile WHERE email = $1"#,
        email
    )
    .fetch_optional(pool)
    .await
}

pub async fn insert_profile(
    payload: &ProfileInsert,
    pool: &Pool<Postgres>,
) -> Result<Uuid, sqlx::Error> {
    query_scalar!(
        r#"
            INSERT INTO profile(full_name, username, email, password, avatar, active)
            VALUES ($1, $2, $3, $4, $5, $6)
            RETURNING id
        "#,
        payload.full_name,
        payload.username,
        payload.email,
        payload.password,
        payload.avatar,
        payload.active
    )
    .fetch_one(pool)
    .await
}

pub async fn update_profile(
    id: &Uuid,
    payload: &ProfileUpdate,
    pool: &Pool<Postgres>,
) -> Result<(), sqlx::Error> {
    query!(
        r#"
            UPDATE profile p
            SET
                updated_at = NOW(),
                full_name = COALESCE($2, p.full_name),
                username = COALESCE($3, p.username),
                email = COALESCE($4, p.email),
                password = COALESCE($5, p.password),
                avatar = COALESCE($6, p.avatar),
                active = COALESCE($7, p.active)
            WHERE id = $1
        "#,
        id,
        payload.full_name,
        payload.username,
        payload.email,
        payload.password,
        payload.avatar,
        payload.active
    )
    .execute(pool)
    .await?;

    Ok(())
}

pub async fn insert_super_admin(
    profile_id: &Uuid,
    pool: &Pool<Postgres>,
) -> Result<(), sqlx::Error> {
    query!(
        r#"INSERT INTO super_admin(profile_id) VALUES ($1)"#,
        profile_id,
    )
    .execute(pool)
    .await?;

    Ok(())
}

pub async fn exists_super_admin(
    profile_id: &Uuid,
    pool: &Pool<Postgres>,
) -> Result<bool, sqlx::Error> {
    Ok(query_scalar!(
        r#"SELECT EXISTS(SELECT 1 FROM super_admin WHERE profile_id=$1)"#,
        profile_id,
    )
    .fetch_one(pool)
    .await?
    .unwrap_or_default())
}

pub async fn insert_follow(
    follower_id: &Uuid,
    following_id: &Uuid,
    pool: &Pool<Postgres>,
) -> Result<(), sqlx::Error> {
    query!(
        r#"INSERT INTO follow(follower_id, following_id) VALUES ($1, $2)"#,
        follower_id,
        following_id,
    )
    .execute(pool)
    .await?;

    Ok(())
}

pub async fn delete_follow(
    follower_id: &Uuid,
    following_id: &Uuid,
    pool: &Pool<Postgres>,
) -> Result<(), sqlx::Error> {
    query!(
        r#"DELETE FROM follow WHERE follower_id = $1 AND following_id = $2"#,
        follower_id,
        following_id,
    )
    .execute(pool)
    .await?;

    Ok(())
}
