# Stonks Backend Assignment

## Setup

To be able to run the project you will need to install the following:

- Rust (https://www.rust-lang.org/tools/install)
- Cargo (comes with Rust) (Rust dependency manager)
- sqlx-cli (`cargo install sqlx-cli`) (Tool to run database migrations)
- PostgreSQL (https://www.postgresql.org/download/)

Then you need to create a `.env` file at the root of the project, you can copy the `.env.example` content and adapt it.

Once you have a database running you can run the following command to set it up and run the migrations:

```shell
sqlx database setup
```

You are now ready to run the project, use the following command (the first compilation will be a little slow):

```shell
cargo run
```

The server will be available at http://localhost:3000 unless you've changed the `.env`.

## The API

The API is accessible under `/api`, if you visit this page http://localhost:3000/api, you will have a Swagger
documentation.

# The WebSocket

You can connect to the websocket here http://localhost:3000/ws. You will first need to join a channel using the
following command:

```text
/join <channel_id>
```

Then you should be able to send message to other users in the same channel, you can test it out with an online tool such
as https://websocketking.com.