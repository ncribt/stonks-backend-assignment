use utoipa::OpenApi;

use crate::auth;
use crate::controller;
use crate::model;

#[derive(OpenApi)]
#[openapi(
    servers((url = "/api")),
    paths(
        controller::profile::get_profile,
        controller::profile::patch_profile,
        controller::profile::post_profile,
        controller::profile::post_follow,
        controller::profile::delete_follow,
        controller::channel::get_channel,
        controller::channel::patch_channel,
        controller::channel::post_channel,
        controller::auth::login,
    ),
    components(schemas(
        model::profile::Profile,
        model::profile::ProfileInsert,
        model::profile::ProfileUpdate,
        model::channel::Channel,
        model::channel::ChannelInsert,
        model::channel::ChannelUpdate,
        controller::profile::PostProfileResponse,
        controller::channel::PostChannelResponse,
        auth::User,
        auth::Credentials,
    ))
)]
pub struct Swagger;
