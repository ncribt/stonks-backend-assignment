BEGIN TRANSACTION;

DROP TABLE follow;

DROP TABLE channel_admin;

DROP TABLE channel_ban;

DROP TABLE channel_mute;

DROP TABLE super_admin;

DROP TABLE mfa;

DROP TABLE channel;

DROP TABLE profile;

END TRANSACTION;