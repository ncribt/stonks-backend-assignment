BEGIN TRANSACTION;

CREATE TABLE profile (
    id UUID DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
    full_name TEXT NOT NULL,
    username TEXT NOT NULL UNIQUE,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    avatar TEXT NOT NULL,
    active BOOLEAN NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE follow (
    id SERIAL NOT NULL PRIMARY KEY,
    follower_id UUID NOT NULL REFERENCES profile(id),
    following_id UUID NOT NULL REFERENCES profile(id),
    CHECK (follower_id <> following_id),
    UNIQUE (follower_id, following_id)
);

CREATE TABLE channel (
    id UUID DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
    title TEXT NOT NULL,
    description TEXT NOT NULL,
    host UUID NOT NULL REFERENCES profile(id),
    suspended BOOLEAN DEFAULT false NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE channel_admin (
    id SERIAL NOT NULL PRIMARY KEY,
    channel_id UUID NOT NULL REFERENCES channel(id),
    profile_id UUID NOT NULL REFERENCES profile(id),
    UNIQUE (channel_id, profile_id)
);

CREATE TABLE channel_ban (
    id SERIAL NOT NULL PRIMARY KEY,
    channel_id UUID NOT NULL REFERENCES channel(id),
    profile_id UUID NOT NULL REFERENCES profile(id),
    UNIQUE (channel_id, profile_id)
);

CREATE TABLE channel_mute (
    id SERIAL NOT NULL PRIMARY KEY,
    channel_id UUID NOT NULL REFERENCES channel(id),
    profile_id UUID NOT NULL REFERENCES profile(id),
    UNIQUE (channel_id, profile_id)
);

CREATE TABLE super_admin (
    id SERIAL NOT NULL PRIMARY KEY,
    profile_id UUID NOT NULL UNIQUE REFERENCES profile(id)
);

CREATE TABLE mfa (
    id SERIAL NOT NULL PRIMARY KEY,
    profile_id UUID NOT NULL UNIQUE REFERENCES profile(id),
    code TEXT,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL
);

END TRANSACTION;