use std::sync::Arc;

use axum::{
    Router,
    routing::{get, post},
};
use axum::routing::patch;
use axum_login::{
    AuthManagerLayerBuilder,
    login_required,
    tower_sessions::{MemoryStore, SessionManagerLayer},
};
use tower_http::trace::{self, TraceLayer};
use tracing::Level;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

use crate::app_state::AppState;
use crate::auth::Backend;
use crate::controller::{
    auth::login,
    channel::{get_channel, patch_channel, post_channel},
    profile::{delete_follow, get_profile, patch_profile, post_follow, post_profile},
    ws::ws_handler,
};
use crate::env::Env;
use crate::swagger::Swagger;

mod app_state;

mod auth;
mod controller;
mod db;
mod env;
mod model;
mod swagger;
mod util;

#[tokio::main]
async fn main() {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();

    let env = Env::from_env();
    let state = Arc::new(AppState::new(&env.database_url).await);

    let session_store = MemoryStore::default();
    let session_layer = SessionManagerLayer::new(session_store);
    let backend = Backend::new(state.pool.clone());
    let auth_layer = AuthManagerLayerBuilder::new(backend, session_layer).build();

    let auth_router = Router::new().route("/login", post(login));

    let profile_router = Router::new()
        .route("/:id", get(get_profile).patch(patch_profile))
        .route("/:id/follow", post(post_follow).delete(delete_follow))
        .route_layer(login_required!(Backend, login_url = "/login"))
        .route("/", post(post_profile));

    let channel_router = Router::new()
        .route("/", post(post_channel))
        .route("/:id", patch(patch_channel))
        .route_layer(login_required!(Backend, login_url = "/login"))
        .route("/:id", get(get_channel));

    let api = Router::new()
        .nest("/", auth_router)
        .nest("/profile", profile_router)
        .nest("/channel", channel_router);

    let app = Router::new()
        .merge(SwaggerUi::new("/api").url("/api/openapi.json", Swagger::openapi()))
        .nest("/api", api)
        .route("/ws", get(ws_handler))
        .with_state(state)
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO))
                .on_response(trace::DefaultOnResponse::new().level(Level::INFO)),
        )
        .layer(auth_layer);

    let listener = tokio::net::TcpListener::bind(format!("{}:{}", env.host, env.port))
        .await
        .unwrap();

    axum::serve(listener, app).await.unwrap();
}
